#!/bin/bash 

#export BLASTDB=/data/tst/contam_chk/db

TIMECMD=/usr/bin/time

if [ $# -lt 2 ]
then
	echo
	echo Prerequisites:
	echo "    BLAST+  (location of executables must be in the PATH)"
	echo "    nt databases (with taxonomy files taxdb.btd and taxdb.bti) in a local directory" 
	echo
	echo Usage:
	echo
	echo "$0 FASTQ_FILE  PATH_TO_NT_DATABASE [E_CUT]"
	echo
	echo where:
	echo 
	echo "FASTQ_FILE is the fastq file, gzipped or not, to be analyzed"
	echo "           (no line breaks allowed within sequence)"
	echo
	echo "PATH_TO_NT_DATABASE is the full path to the local directory containing the NCBI nt"
	echo "                    database (files nt.*) and the taxonomy files taxdb.btd and taxdb.bti"
	echo
	echo "ECUT is the maximum BLAST e-value for significant hit detection (default: 1e-20)"
	echo
	echo Output will be written to file called FASTQ_FILE.species_stats
	echo
	exit
fi

FASTQ=$1
BLASTDB=$2
export BLASTDB

SAMPREADS=500

ECUT="1e-20"
if [ $# -eq 3 ]
then
	ECUT=$3
fi

SCRIPTDIR=`dirname "$BASH_SOURCE"`

echo Subsampling fastq
$TIMECMD $SCRIPTDIR/subsample_fastq.pl $FASTQ $SAMPREADS fasta > $FASTQ.tst.fa

echo Runing BLAST
#blastn -num_threads 5 -db $BLASTDB/nt -query $FASTQ.tst.fa -out $FASTQ.blast  -outfmt "6 std staxids sscinames scomnames sblastnames"
$TIMECMD blastn -max_hsps 1 -max_target_seqs 1 -db $BLASTDB/nt -query $FASTQ.tst.fa -out $FASTQ.blast  -outfmt "6 std staxids sscinames scomnames sblastnames"
sort -k 1,1 -k 12,12nr $FASTQ.blast > $FASTQ.blast.sorted
mv $FASTQ.blast.sorted $FASTQ.blast

echo Counting reads
echo Species detected in file $FASTQ > $FASTQ.species_stats
echo based on $SAMPREADS uniformly sampled reads >> $FASTQ.species_stats
$TIMECMD $SCRIPTDIR/count_species.pl $FASTQ.blast $ECUT >> $FASTQ.species_stats 
