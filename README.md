### What is fastq_species_detector for? ###

The pipeline runs BLAST on 500 reads sampled uniformly from a fastq file
lane against **nt** database and for each read it records the species of the
best hit. The output is a table showing the number and percentage of reads
for each species detected.


### How do I get set up? ###

* Installation
	* Clone the pipeline into a directory of your choice
	* Copy ther NCBI nt databases with taxonomy files taxdb.btd and taxdb.bti into a (preferably local) directory

* Dependencies (in the PATH): 
	* BLAST+
	* perl

 
### How to run ###

To run the pipeline on a single fastq file, enter the following command in the directory where the file is located:

    /path/to/scripts/fastq_species_detector.sh some_file.fastq /path/to/nt_database [ECUT]


(`.gz` and `.bz2` compressed versions of fastq files are also accepted - no need to decompress such files before use)

### Output example:


```
Species detected in file example_R1.fastq.gz
based on 500 uniformly sampled reads
--------------------------------------
Table below reports the numbers and percentages
of reads with best BLAST hit to a given species
--------------------------------------
Total number of reads with BLAST hits to nt (e-value<1e-20): 402
--------------------------------------

Read distribution over species:

Species #Reads  %Reads
--------------------------------------
Zea mays        356     88.557
Zea perennis    7       1.741
Zea mays subsp. mays    7       1.741
Zea diploperennis       6       1.493
Sorghum bicolor 5       1.244
Zea mays subsp. parviglumis     3       0.746
Zea mays subsp. mexicana        3       0.746
Chrysemys picta bellii  2       0.498
Zea luxurians   2       0.498
Tripsacum dactyloides   1       0.249
Oryza sativa Indica Group       1       0.249
Enterobacteria phage phiX174    1       0.249
Acinetobacter baumannii AC30    1       0.249
Paradoxosomatidae sp. YG-2006   1       0.249
synthetic construct     1       0.249
Oryza sativa Japonica Group     1       0.249
Setaria italica 1       0.249
Hymenachne donacifolia  1       0.249
mini-chromosome MMC1    1       0.249
-----------------------------------
```

### Contact information ###

* Robert Bukowski (bukowski@cornell.edu)
* Bioinformatics Facilty, Cornell University