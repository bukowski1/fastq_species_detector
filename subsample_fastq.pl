#!/usr/local/bin/perl

# Take a fastq file and select $reqseq reads extracted uniformly
# from the whole file

$file=$ARGV[0];
$reqseq=$ARGV[1];
$outformat = lc $ARGV[2]; # may be fastq or fasta

$iscompr = 0;

# Uncompress (if needed) and count sequences in the file
if($file =~ m/\.gz$/)
{
        $iscompr = 1;
        $rawfile = $file;
        $rawfile =~ s/\.gz//;
        `gunzip -c $file > $rawfile`;
	$nseq = `wc -l $rawfile`;
        $file = $rawfile;
}
elsif($file =~ m/\.bz2$/)
{
        $iscompr = 1;
	`bunzip2 --keep $file`;
        $file =~ s/\.bz2//;
        $nseq = `wc -l $file`;
}
else
{
	$nseq = `wc -l $file`;
}
chomp $nseq;
$nseq /= 4;

$stride = int($nseq/$reqseq);

#print "$reqseq out of $nseq sequences will be selected using stride $stride\n";

# $file is now always the uncompressed file
open(in,$file);
$counter=0;
$countext = 0;
while($line1=<in>)
{
	$line2=<in>;
	$line3=<in>;
	$line4=<in>;

	if($counter % $stride == 0)
	{
		if($outformat eq "fasta")
		{
			$line1 =~ s/\@/>/;
		}
		print $line1;
		print $line2;
		if($outformat eq "fastq")
                {
			print $line3;
			print $line4;
		}
		$countext++;
	}

	$counter++;
}
close in;

# Remove the uncompressed version if the original was compressed
if($iscompr == 1)
{
  unlink $file; 
}

#print "$countext sequences extracted\n";
